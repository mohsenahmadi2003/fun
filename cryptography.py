def encrypt():
    string = input()
    value = int(input())
    encrypt = []

    for character in string:
        ascii = ord(character)
        for count in range(value):
            if ascii == 122:
                ascii = 96
            if ascii == 90:
                ascii = 64
            ascii = ascii + 1
        encrypt.append(ascii)
            
    for asciiCode in encrypt:
        print(chr(asciiCode),end="")
    print() 

def decrypt():
    
    string = input()
    value = int(input())
    decrypt = []

    for character in string:
        ascii = ord(character)
        for count in range(value):
            if ascii == 97:
                ascii = 123
            if ascii == 65:
                ascii = 91
            ascii = ascii - 1
        decrypt.append(ascii)
            
    for asciiCode in decrypt:
        print(chr(asciiCode),end="") 

encrypt()
decrypt()

